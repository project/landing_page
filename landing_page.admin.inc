<?php

/**
 * List of admin menus created.
 */
function landing_page_overview() {
  $output = '';
  $rows = array();
  $head = array(t('Path'), t('Title'), '');
  
  $result = db_query('SELECT page_id, title, description, path FROM {landing_page} ORDER BY path');
  
  while ($data = db_fetch_array($result)) {
    $rows[] = array(
      (strpos($data['path'], '%') !== FALSE) ? check_plain($data['path']) : l($data['path'], $data['path']),
      check_plain($data['title']),
      l(t('Edit'), 'admin/settings/landing_page/'. $data['page_id'] .'/edit', array('query' => drupal_get_destination())) . ' ' . l(t('Delete'), 'admin/settings/landing_page/'. $data['page_id'] .'/delete', array('query' => drupal_get_destination())),
    );
  }
  
  if (!empty($rows)) {
    $output = theme('table', $head, $rows);
  }
  else {
    $output = t('There are currently no landing pages set up.');
  }
  
  return $output;
}

/**
 * Landing page edit form
 */
function landing_page_form(&$form_state, $page = array()) {
  $page+= array(
    'title' => '', 'description' => '', 'roles' => array(), 'type' => MENU_NORMAL_ITEM,
    'weight' => 0, 'menu' => 'navigation',
  );
  $form = array();
  
  if (isset($page['page_id'])) {
    $form['page_id'] = array(
      '#type' => 'value',
      '#value' => $page['page_id'],
    );
  }
  
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $page['path'],
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Type'),
    '#default_value' => $page['type'],
    '#options' => array(
      MENU_CALLBACK => t('No Menu'),
      MENU_NORMAL_ITEM => t('Normal menu item'),
      MENU_DEFAULT_LOCAL_TASK => t('Default local task'),
      MENU_LOCAL_TASK => t('Local task'),
    ),
    '#required' => TRUE,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $page['title'],
    '#required' => TRUE,
  );
  $form['descripion'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $page['description'],
  );
  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => $page['roles'],
    '#options' => user_roles(),
  );
  $form['menu'] = array(
    '#type' => 'radios',
    '#title' => t('Menu'),
    '#default_value' => $page['menu'],
    '#options' => menu_get_menus(),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $page['weight'],
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

function landing_page_form_submit(&$form, &$form_state) {
  if (isset($form_state['values']['page_id'])) {
    drupal_set_message(t('Landing page %name has been created', array('%name' => $form_state['values']['title'])));
  }
  else {
    drupal_set_message(t('Landing page %name has been updated', array('%name' => $form_state['values']['title'])));
  }
  
  landing_page_save($form_state['values']);
  $form_state['redirect'] = 'admin/settings/landing_page';
  
  menu_rebuild();
}

function landing_page_delete_confirm(&$form_state, $page) {
  $form = array();
  
  $form['page_id'] = array(
    '#type' => 'value',
    '#value' => $page['page_id'],
  );
  $form['title'] = array(
    '#type' => 'value',
    '#value' => $page['title'],
  );
  
  return confirm_form($form,
    t('Delete %name landing page', array('%name' => $page['title'])),
    'admin/settings/landing_page'
  );
}

function landing_page_delete_confirm_submit(&$form, &$form_state) {
  db_query('DELETE FROM {landing_page} WHERE page_id = %d', $form_state['values']['page_id']);
  $form_state['redirect'] = 'admin/settings/landing_page';
  drupal_set_message(t('Landing page %name has been deleted', array('%name' => $form_state['values']['title'])));
  
  menu_rebuild();
}