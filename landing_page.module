<?php

/**
 * @file
 * Landing page module
 */

/**
 * Implementation of hook_menu().
 */
function landing_page_menu() {
  $items = array();
  
  $items['admin/settings/landing_page'] = array(
    'title' => 'Landing Pages',
    'description' => 'Allows the creation of simple landing pages to be loaded onto the system.',
    'access arguments' => array('access administration'),
    'page callback' => 'landing_page_overview',
    'file' => 'landing_page.admin.inc'
  );
  $items['admin/settings/landing_page/list'] = array(
    'title' => 'List',
    'access arguments' => array('access administration'),
    'page callback' => 'landing_page_overview',
    'file' => 'landing_page.admin.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -1,
  );
  $items['admin/settings/landing_page/add'] = array(
    'title' => 'Add',
    'access arguments' => array('access administration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('landing_page_form'),
    'file' => 'landing_page.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/settings/landing_page/%landing_page/edit'] = array(
    'title' => 'Edit',
    'access arguments' => array('access administration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('landing_page_form', 3),
    'file' => 'landing_page.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/landing_page/%landing_page/delete'] = array(
    'title' => 'Delete',
    'access arguments' => array('access administration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('landing_page_delete_confirm', 3),
    'file' => 'landing_page.admin.inc',
    'type' => MENU_CALLBACK,
  );
  
  $results = db_query('SELECT page_id FROM {landing_page}');
  while ($page = db_fetch_array($results)) {
    $page = landing_page_load($page['page_id']);
    
    $items[$page['path']] = array(
      'title' => $page['title'],
      'description' => $page['description'],
      'weight' => $page['weight'],
      'type' => $page['type'],
      'page callback' => 'system_admin_menu_block_page',
      'access callback' => 'landing_page_access',
      'access arguments' => $page['roles'],
      'file' => 'system.admin.inc',
      'file path' => drupal_get_path('module', 'system'),
      'menu_name' => $page['menu'],
    );
  }
  
  return $items;
}

function landing_page_access($roles) {
  global $user;
  
  $roles = array_filter($roles);
  
  return ($user->uid == 1 || array_intersect($roles, $user->roles)) ? TRUE : FALSE;
}

/**
 * Load the landing page
 */
function landing_page_load($page_id) {
  if ($page = db_fetch_array(db_query('SELECT * FROM {landing_page} WHERE page_id = %d', $page_id))) {
    $page['roles'] = unserialize($page['roles']);
    
    return $page;
  }
  return FALSE;
}

/**
 * Save the landing page
 */
function landing_page_save(&$page) {
  if (isset($page['page_id'])) {
    drupal_write_record('landing_page', $page, 'page_id');
  }
  
  if (!isset($page['page_id']) || !db_affected_rows()) {
    drupal_write_record('landing_page', $page);
  }
}